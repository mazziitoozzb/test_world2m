# TEST WORLD 2 MEET ROBERTO MAZO FERNANDEZ

## ANGULAR server

Ejecute `ng serve` para lanzar el servidor de desarrollo. Navegue a `http://localhost:4200/`. La aplicación se recargará automáticamente si cambia alguno de los archivos de origen.

## MOCKSERVER

Ejecute `npm run mock-server` para lanzar el mockserver en `http://localhost:3000`. Las url disponibles se pueden ver en /`mockserver/routes.json` y la database ficticia en `database.json`

## TEST UNITARIOS DEL SERVICE

Ejecute `ng test` para ejecutar los test del `superhero.service.ts`
