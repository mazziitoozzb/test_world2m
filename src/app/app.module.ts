import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SuperheroListComponent } from './components/home/superhero-list/superhero-list.component';
import { MaterialModule } from './material.module';
import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmationHeroDeleteDialogComponent } from './components/home/dialogs/confirmation-hero-delete-dialog/confirmation-hero-delete-dialog.component';
import { AddEditSuperheroDialogComponent } from './components/home/dialogs/add-edit-superhero-dialog/add-edit-superhero-dialog.component';
import { UppercaseDirective } from './directives/uppercase.directive';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { LoadingService } from './services/loading.service';
import { HeaderComponent } from './components/layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    SuperheroListComponent,
    HomeComponent,
    ConfirmationHeroDeleteDialogComponent,
    AddEditSuperheroDialogComponent,
    UppercaseDirective,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [
    LoadingService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
