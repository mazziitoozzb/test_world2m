import { Injectable } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SuperheroService {
  private apiUrl = 'http://localhost:3000/api/superheroes';

  constructor(private http: HttpClient) {}

  public getSuperheroes(): Observable<Superhero[]> {
    return this.http.get<Superhero[]>(this.apiUrl);
  }

  public getSuperheroById(id: number): Observable<Superhero> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Superhero>(url);
  }

  public searchSuperheroesByName(name: string): Observable<Superhero[]> {
    return this.http
      .get<Superhero[]>(this.apiUrl)
      .pipe(
        map((superheroes: Superhero[]) =>
          superheroes.filter((hero) =>
            hero.name.toLowerCase().includes(name.toLowerCase())
          )
        )
      );
  }

  public addSuperhero(superhero: any): Observable<Superhero> {
    return this.http.post<Superhero>(this.apiUrl, superhero);
  }

  public updateSuperhero(superhero: Superhero): Observable<Superhero> {
    const url = `${this.apiUrl}/${superhero.id}`;
    return this.http.put<Superhero>(url, superhero);
  }

  public deleteSuperhero(id: number): Observable<void> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete<void>(url);
  }
}
