import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { SuperheroService } from './superhero.service';
import { Superhero } from '../models/superhero.model';

describe('SuperheroService', () => {
  let service: SuperheroService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SuperheroService],
    });
    service = TestBed.inject(SuperheroService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get superheroes', () => {
    const mockSuperheroes: Superhero[] = [
      { id: 1, name: 'Spiderman', strength: 88 },
      { id: 2, name: 'Superman', strength: 85 },
    ];

    service.getSuperheroes().subscribe((superheroes) => {
      expect(superheroes).toEqual(mockSuperheroes);
    });

    const req = httpMock.expectOne('http://localhost:3000/api/superheroes');
    expect(req.request.method).toBe('GET');
    req.flush(mockSuperheroes);
  });

  it('should get superhero by id', () => {
    const mockSuperhero: Superhero = { id: 1, name: 'Spiderman', strength: 88 };

    service.getSuperheroById(1).subscribe((superhero) => {
      expect(superhero).toEqual(mockSuperhero);
    });

    const req = httpMock.expectOne('http://localhost:3000/api/superheroes/1');
    expect(req.request.method).toBe('GET');
    req.flush(mockSuperhero);
  });
  it('should update superhero', () => {
    const superheroToUpdate: Superhero = {
      id: 1,
      name: 'Spiderman',
      strength: 90,
    };

    service.updateSuperhero(superheroToUpdate).subscribe((updatedSuperhero) => {
      expect(updatedSuperhero).toEqual(superheroToUpdate);
    });

    const req = httpMock.expectOne('http://localhost:3000/api/superheroes/1');
    expect(req.request.method).toBe('PUT');
    expect(req.request.body).toEqual(superheroToUpdate);
    req.flush(superheroToUpdate);
  });

  it('should add superhero', () => {
    const newSuperhero: Superhero = { id: 3, name: 'Iron Man', strength: 95 };

    service.addSuperhero(newSuperhero).subscribe((addedSuperhero) => {
      expect(addedSuperhero).toEqual(newSuperhero);
    });

    const req = httpMock.expectOne('http://localhost:3000/api/superheroes');
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(newSuperhero);
    req.flush(newSuperhero);
  });

  it('should delete superhero', () => {
    const superheroId = 2;

    service.deleteSuperhero(superheroId).subscribe(() => {
      expect().nothing();
    });

    const req = httpMock.expectOne('http://localhost:3000/api/superheroes/2');
    expect(req.request.method).toBe('DELETE');
    req.flush(null);
  });
});
