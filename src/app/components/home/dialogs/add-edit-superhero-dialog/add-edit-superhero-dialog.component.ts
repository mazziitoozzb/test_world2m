import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Superhero } from 'src/app/models/superhero.model';
import { SuperheroService } from 'src/app/services/superhero.service';

@Component({
  selector: 'app-add-edit-superhero-dialog',
  templateUrl: './add-edit-superhero-dialog.component.html',
  styleUrls: ['./add-edit-superhero-dialog.component.scss'],
})
export class AddEditSuperheroDialogComponent implements OnInit {
  editForm!: FormGroup;

  get formControls() {
    return this.editForm.controls;
  }

  constructor(
    public dialogRef: MatDialogRef<AddEditSuperheroDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { superhero: Superhero },
    private formBuilder: FormBuilder,
    private superheroSvc: SuperheroService
  ) {}

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      name: [
        this.data ? this.data.superhero.name.toUpperCase() : '',
        Validators.required,
      ],
      strength: [
        this.data ? this.data.superhero.strength : '',
        Validators.required,
      ],
    });
  }

  cancel(): void {
    this.dialogRef.close();
  }
  addSuperhero(): void {
    const hero = {
      name: this.formControls['name'].value,
      strength: this.formControls['strength'].value,
    };
    this.superheroSvc
      .addSuperhero(hero)
      .subscribe((res) => this.dialogRef.close(true));
  }

  editSuperhero(): void {
    if (this.editForm.invalid) {
      return;
    }

    const editedHero: Superhero = {
      id: this.data.superhero.id,
      name: this.formControls['name'].value,
      strength: this.formControls['strength'].value,
    };
    this.superheroSvc.updateSuperhero(editedHero).subscribe((res) => {
      this.dialogRef.close(true);
    });
  }
}
