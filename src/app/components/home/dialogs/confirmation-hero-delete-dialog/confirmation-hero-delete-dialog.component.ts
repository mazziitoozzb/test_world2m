import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Superhero } from 'src/app/models/superhero.model';
import { SuperheroService } from 'src/app/services/superhero.service';

@Component({
  selector: 'app-confirmation-hero-delete-dialog',
  templateUrl: './confirmation-hero-delete-dialog.component.html',
  styleUrls: ['./confirmation-hero-delete-dialog.component.scss'],
})
export class ConfirmationHeroDeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { superhero: Superhero },
    private superheroSvc: SuperheroService
  ) {}

  ngOnInit(): void {}

  deleteSuperhero() {
    this.superheroSvc.deleteSuperhero(this.data.superhero.id).subscribe();
  }
}
