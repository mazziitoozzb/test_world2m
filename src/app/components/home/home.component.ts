import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  loading: boolean = false;
  constructor(private loadingSvc: LoadingService) {}

  ngOnInit(): void {
    this.loadingSvc.loading$.subscribe((loading) => {
      this.loading = loading;
    });
  }
}
