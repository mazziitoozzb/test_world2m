import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Superhero } from '../../../models/superhero.model';
import { SuperheroService } from 'src/app/services/superhero.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationHeroDeleteDialogComponent } from '../dialogs/confirmation-hero-delete-dialog/confirmation-hero-delete-dialog.component';
import { AddEditSuperheroDialogComponent } from '../dialogs/add-edit-superhero-dialog/add-edit-superhero-dialog.component';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ViewChild } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
@Component({
  selector: 'app-superhero-list',
  templateUrl: './superhero-list.component.html',
  styleUrls: ['./superhero-list.component.scss'],
})
export class SuperheroListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  superheroForm!: FormGroup;

  displayedColumns: string[] = ['id', 'name', 'strength', 'actions'];
  filteredSuperheroes: Superhero[] = [];

  constructor(
    private dialog: MatDialog,
    private superheroSvc: SuperheroService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.superheroSvc.getSuperheroes().subscribe((superheroes) => {
      this.filteredSuperheroes = superheroes;
      this.initPaginator();
    });
  }

  private async initPaginator(): Promise<void> {
    this.paginator.length = this.filteredSuperheroes.length;
    this.paginator.pageSize = 5;
    this.filteredSuperheroes = await this.getSuperHeroesPaginated();
    this.paginator.page.subscribe(async (event: PageEvent) => {
      const startIndex = event.pageIndex * event.pageSize;
      const endIndex = startIndex + event.pageSize;
      this.filteredSuperheroes = (
        await firstValueFrom(this.superheroSvc.getSuperheroes())
      ).slice(startIndex, endIndex);
    });
    this.changeDetectorRef.detectChanges();
  }

  private async getSuperHeroesPaginated() {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    const endIndex = startIndex + this.paginator.pageSize;
    const superheroes = await firstValueFrom(
      this.superheroSvc.getSuperheroes()
    );
    return superheroes.slice(startIndex, endIndex);
  }

  private initForm(): void {
    this.superheroForm = this.formBuilder.group({
      name: [''],
      id: [''],
    });
    this.superheroForm.get('name')?.valueChanges.subscribe((value) => {
      this.searchByName(value);
    });
    this.superheroForm.get('id')?.valueChanges.subscribe((value) => {
      this.searchById(value);
    });
  }

  public addSuperhero(): void {
    const dialogRef = this.dialog.open(AddEditSuperheroDialogComponent, {
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.superheroSvc.getSuperheroes().subscribe((superheroes) => {
          this.filteredSuperheroes = superheroes;
        });
      }
    });
  }

  public editSuperhero(superhero: Superhero): void {
    const dialogRef = this.dialog.open(AddEditSuperheroDialogComponent, {
      width: '600px',
      data: { superhero: superhero },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.superheroSvc.getSuperheroes().subscribe((superheroes) => {
          this.filteredSuperheroes = superheroes;
        });
      }
    });
  }

  public deleteSuperhero(superhero: Superhero): void {
    const dialogRef = this.dialog.open(ConfirmationHeroDeleteDialogComponent, {
      width: '600px',
      data: { superhero: superhero },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.superheroSvc.getSuperheroes().subscribe((superheroes) => {
          this.filteredSuperheroes = superheroes;
        });
      }
    });
  }

  private async searchByName(name: string): Promise<void> {
    if (name) {
      this.superheroSvc
        .searchSuperheroesByName(name)
        .pipe(
          catchError((error) => {
            console.error('Error al obtener el superheroe:', error);
            return of(null);
          })
        )
        .subscribe((superheros) => {
          this.filteredSuperheroes = superheros || [];
        });
    } else {
      this.filteredSuperheroes = await this.getSuperHeroesPaginated();
    }
  }

  private async searchById(id: number): Promise<void> {
    if (id) {
      this.superheroSvc
        .getSuperheroById(Number(id))
        .pipe(
          catchError((error) => {
            console.error('Error al obtener el superhéroe:', error);
            return of(null);
          })
        )
        .subscribe((superhero) => {
          this.filteredSuperheroes = superhero ? [superhero] : [];
        });
    } else {
      this.filteredSuperheroes = await this.getSuperHeroesPaginated();
    }
  }
}
