export interface Superhero {
  id: number;
  name: string;
  strength: number;
}
